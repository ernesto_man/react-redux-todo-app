import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

import todoApp from './reducers/reducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(todoApp);
export default store;

ReactDOM.render(
<Provider store={store}>
<App />
</Provider>,
 document.getElementById('root')
 );
 
registerServiceWorker();
