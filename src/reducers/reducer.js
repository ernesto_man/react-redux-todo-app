import {ADD_TASK, SET_FILTER, TOGGLE_TASK} from '../actions/actions';


const initialState = {
	visibilityFilter: 'ALL',
	todos: [
		/*{index: 1, text: 'task1', complited: false},
		{index: 2, text: 'task2', complited: false}*/]
}

export default function todoApp(state = initialState, action) {
	switch (action.type) {
		case ADD_TASK:
		return Object.assign({}, state, {
			todos: [
			...state.todos,
			{
				index: action.index,
				text: action.text,
				completed: false
			}
		]
		});

		case TOGGLE_TASK:
		return Object.assign ({}, state, {
			todos: state.todos.map((todo, index) => {
				if (index === action.index) {
					console.log(state);
					return Object.assign({}, todo, {
						completed: !todo.completed
					});
				}
				return todo;
			})
		});


		case SET_FILTER:
			return Object.assign({}, state, {
				visibilityFilter: action.filter
			});

	default:
		return state;
	}

}
