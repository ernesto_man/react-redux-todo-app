import React from 'react';

import Radio from 'arui-feather/radio';
import RadioGroup from 'arui-feather/radio-group';

const Filter = ({onClick}) => (
	<div key='m' className='row'>
		<RadioGroup
			error=''
			label='Фильтр'
			size='m'
			type='button'
		>

			{
				['ALL', 'OPEN', 'DONE'].map(text => (
					<Radio
						key={ text }
						size='m'
						text={ text }
						type='button'
						value={ text }
						onChange= {(text) => onClick(text)}
					/>
				))
			}
		</RadioGroup>
    </div>
)

export default Filter