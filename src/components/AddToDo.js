import React from 'react';

import Input from 'arui-feather/input';
import Button from 'arui-feather/button';

import { connect } from 'react-redux';
import { addTask } from '../actions/actions'

import store from '../index';

const AddToDo = ({ dispatch }) => {
    let input;
	
	return (
	<div className='row' key='m'>
		<Input
			placeholder='Введите что-нибудь'
			view='line'
			size='m'
			ref={node => input = node}
			id='taskInput'
		/>
		<Button onClick = { () => (dispatch(addTask(document.getElementById('taskInput').value, false)), console.log(store.getState())) } size='m'>{ 'Добавить' }</Button>
    </div>
	)
}

export default connect()(AddToDo);