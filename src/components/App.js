import React, { Component } from 'react';
import { createStore } from 'redux';
import todoApp from '../reducers/reducer';

import Header from './Header';
import AddToDo from './AddToDo';
import VisibleFilter from '../containers/VisibleFilter';
import VisibleToDoList from '../containers/VisibleToDoList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <AddToDo />
		<VisibleFilter />
		<VisibleToDoList />
	</div>
    );
  }
}

export default App;
