import React from 'react';

import Label from 'arui-feather/label';
import CheckBox from 'arui-feather/checkbox';
import CheckBoxGroup from 'arui-feather/checkbox-group';

const ToDoList = ({ todos, toggleTask}) => (
	<div key='m' className='row'>
		<CheckBoxGroup label={ <Label size='m'>Задачи</Label> }>

		{todos.map(todo =>
			<CheckBox
				id={'task' + todo.index}
				text={todo.text}
				value={todo.text}
				checked={ todo.completed }
				onChange={() => {toggleTask(todo.index) }}

			/>

		)}

		</CheckBoxGroup>
	</div>
)

export default ToDoList;
