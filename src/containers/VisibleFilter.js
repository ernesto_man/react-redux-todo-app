/*container component*/
import { connect } from 'react-redux';
import Filter from '../components/Filter';
import { setFilter } from '../actions/actions';
import store from '../index';

const mapStateToProps = (state, ownProps) => ({
	active: ownProps.filter === state.visibilityFilter
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	onClick: (filter) => dispatch(setFilter(filter))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Filter)