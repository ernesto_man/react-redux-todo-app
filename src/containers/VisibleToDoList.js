import { connect } from 'react-redux';
import { toggleTask } from '../actions/actions';
import ToDoList from '../components/ToDoList';

const getVisibleToDos = (todos, filter) => {
	switch (filter) {
    case 'ALL':
      return todos
    case 'DONE':
      return todos.filter(t => t.completed)
    case 'OPEN':
      return todos.filter(t => !t.completed)
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
	todos: getVisibleToDos(state.todos, state.visibilityFilter)
})

const mapDispatchToProps = dispatch => ({
	toggleTask: id => dispatch(toggleTask(id))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ToDoList);