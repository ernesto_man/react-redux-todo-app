//set actionTypes
export const ADD_TASK = 'ADD_TASK';
export const SET_FILTER = 'SET_FILTER';
export const TOGGLE_TASK = 'TOGGLE_TASK';

let nextTodoId = 0;

export const addTask = function(text, completed) {
	return {
		type: ADD_TASK,
		index: nextTodoId++,
		text: text,
		completed: completed
	}
}

export const setFilter = function(filter) {
	return {
		type: SET_FILTER,
		filter: filter
	}
}

export const toggleTask = function(index) {
	return {
		type: TOGGLE_TASK,
		index: index
	}
}
